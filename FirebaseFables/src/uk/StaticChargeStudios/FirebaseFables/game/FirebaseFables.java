package uk.StaticChargeStudios.FirebaseFables.game;

import uk.StaticChargeStudios.Framework.Classes.AndroidGame;
import uk.StaticChargeStudios.Framework.Interfaces.Screen;

public class FirebaseFables extends AndroidGame {

	@Override
	public Screen getInitScreen() {
		// TODO Auto-generated method stub
		return new SplashLoadingScreen(this);
	}

	@Override
	public void onBackPressed() {
		getCurrentScreen().backButton();
	}

}
