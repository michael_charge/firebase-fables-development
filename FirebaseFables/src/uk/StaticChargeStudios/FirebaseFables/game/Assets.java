package uk.StaticChargeStudios.FirebaseFables.game;

import android.graphics.Paint;
import uk.StaticChargeStudios.Framework.Interfaces.Game;
import uk.StaticChargeStudios.Framework.Interfaces.Image;
import uk.StaticChargeStudios.Framework.Interfaces.Music;
import uk.StaticChargeStudios.Framework.Interfaces.Sound;

public class Assets {
    
    /// Game Modifiers
    public static BaseType currentBase;
	public static CommanderType currentCommander;
	// Testing Modifier
	public static boolean isTesting;

	/// Enums used across multiple classes
    // Enum for Base Types
	enum BaseType {
        MortarBase, Base105, Base155, BaseSOG
    }
	// Enum for Commander Types
    enum CommanderType {
        InfantryCommander, ArtyCommander, SOGCommander
    }
    // Enum for Map Types
    enum MapSymbolType {
    	LSArtyTarget,SSVietcong,SSHuey,SSPatrol
    }
    
	/// Images
	// Backgrounds
	public static Image splash, menu, ingameBG, levelLoad;
	
	// Unit Graphics
	public static Image LSArtyTarget, SSVietcong, SSHuey, SSPatrol;
	
	// Base type images
	public static Image base155, base105, baseMortar, baseSOG;
	
	// Commander Type images
	public static Image commanderInfantry, commanderArty, commanderSOG;

    // Button Graphics
    public static Image PlayButton, OptionsButton, CreditsButton;
	
	// Text details
	public static Paint textPaint, titlePaint;
	
	/// Audio
	// Music
	public static Music menuMusic;
	// Sound Clip
	public static Sound testClip;
	

	public static void loadMenuMusic(Game game) {
		menuMusic = game.getAudio().createMusic("menu.mp3");
		menuMusic.setLooping(true);
		menuMusic.setVolume(50);
	}
}
