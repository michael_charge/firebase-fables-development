package uk.StaticChargeStudios.FirebaseFables.game;

import java.util.List;

import uk.StaticChargeStudios.Framework.Interfaces.Game;
import uk.StaticChargeStudios.Framework.Interfaces.Graphics;
import uk.StaticChargeStudios.Framework.Interfaces.Screen;
import uk.StaticChargeStudios.Framework.Interfaces.Input.TouchEvent;

public class MainMenuScreen extends Screen {

    enum MenuState {
        FrontMenu, Options, Credits, BaseSelect, CommanderSelect
    }

    MenuState state = MenuState.FrontMenu;

    int tempSelectedBase, tempSelectedCommander;

    public MainMenuScreen(Game game) {
        super(game);

        if (Assets.menuMusic != null) {
            Assets.menuMusic.play();
        }
    }

    @Override
    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

        switch (state) {
            case FrontMenu:
                update_FrontMenu(touchEvents, deltaTime);
                break;
            case Options:
                update_Options(touchEvents, deltaTime);
                break;
            case Credits:
                update_Credits(touchEvents, deltaTime);
                break;
            case BaseSelect:
                update_BaseSelect(touchEvents, deltaTime);
                break;
            case CommanderSelect:
                update_CommanderSelect(touchEvents, deltaTime);
                break;
            default:
                update_Error(deltaTime);
                break;
        }
    }

    private void update_Error(float deltaTime) {
        // TODO Auto-generated method stub

    }

    private void update_FrontMenu(List<TouchEvent> touchEvents, float deltaTime) {
        // TODO Auto-generated method stub
        int len = touchEvents.size();
        for (int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if (event.type == TouchEvent.TOUCH_UP) {
                if (event.y < 300 && event.y > 200) {
                    state = MenuState.BaseSelect;
                } else if (event.y < 450 && event.y > 350) {
                    state = MenuState.Options;
                } else if (event.y < 600 && event.y > 500) {
                    state = MenuState.Credits;
                }

            }
        }

    }

    private void update_CommanderSelect(List<TouchEvent> touchEvents, float deltaTime) {
        int len = touchEvents.size();
        for (int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if (event.type == TouchEvent.TOUCH_UP) {
                if (event.y < 600 && event.y > 300 && event.x < 250 && event.x > 100) {
                    Assets.currentCommander = Assets.CommanderType.InfantryCommander;
                    game.setScreen(new GameScreen(game));
                } else if (event.y < 600 && event.y > 300 && event.x < 750 && event.x > 500) {
                    Assets.currentCommander = Assets.CommanderType.ArtyCommander;
                    game.setScreen(new GameScreen(game));
                } else if (event.y < 600 && event.y > 300 && event.x < 1150 && event.x > 900) {
                    Assets.currentCommander = Assets.CommanderType.SOGCommander;
                    game.setScreen(new GameScreen(game));
                }
            }
        }
    }

    private void update_Options(List<TouchEvent> touchEvents, float deltaTime) {
        // TODO Auto-generated method stub

    }

    private void update_Credits(List<TouchEvent> touchEvents, float deltaTime) {
        // TODO Auto-generated method stub

    }

    private void update_BaseSelect(List<TouchEvent> touchEvents, float deltaTime) {
        int len = touchEvents.size();
        for (int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if (event.type == TouchEvent.TOUCH_UP) {
                if (event.y < 600 && event.y > 300 && event.x < 325 && event.x > 75) {
                    Assets.currentBase = Assets.BaseType.MortarBase;
                    state = MenuState.CommanderSelect;
                } else if (event.y < 600 && event.y > 300 && event.x < 625 && event.x > 375) {
                    Assets.currentBase = Assets.BaseType.Base105;
                    state = MenuState.CommanderSelect;
                } else if (event.y < 600 && event.y > 300 && event.x < 925 && event.x > 675) {
                    Assets.currentBase = Assets.BaseType.Base155;
                    state = MenuState.CommanderSelect;
                } else if (event.y < 600 && event.y > 300 && event.x < 1225 && event.x > 975) {
                    Assets.currentBase = Assets.BaseType.BaseSOG;
                    state = MenuState.CommanderSelect;
                }
            }
        }
    }

    @Override
    public void draw(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawImage(Assets.menu, 0, 0);

        if (Assets.menuMusic.isPlaying())
            g.drawString("Music is Working", 20, 20, Assets.textPaint);

        switch (state) {
            case FrontMenu:
                draw_FrontMenu(deltaTime);
                break;
            case Options:
                draw_Options(deltaTime);
                break;
            case Credits:
                draw_Credits(deltaTime);
                break;
            case BaseSelect:
                draw_BaseSelect(deltaTime);
                break;
            case CommanderSelect:
                draw_CommanderSelect(deltaTime);
                break;
            default:
                draw_Error(deltaTime);
                break;
        }

    }

    private void draw_Error(float deltaTime) {
        // TODO Auto-generated method stub
        Graphics g = game.getGraphics();
        g.drawString("Sorry but something isn't quite right, please contact the developer with what you were doing to cause this", 20, 360, Assets.textPaint);
    }

    private void draw_CommanderSelect(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawString("Commander Select", 150, 150, Assets.textPaint);

        g.drawImage(Assets.commanderInfantry, 100, 300);
        g.drawString("Infantry", 100, 700, Assets.textPaint);
        g.drawImage(Assets.commanderArty, 500, 300);
        g.drawString("Artillery", 500, 700, Assets.textPaint);
        g.drawImage(Assets.commanderSOG, 900, 300);
        g.drawString("SOG", 900, 700, Assets.textPaint);

    }

    private void draw_BaseSelect(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawString("Base Select", 150, 150, Assets.textPaint);

        g.drawImage(Assets.baseMortar, 75, 300);
        g.drawString("Mortar", 100, 700, Assets.textPaint);
        g.drawImage(Assets.base105, 375, 300);
        g.drawString("105mm", 375, 700, Assets.textPaint);
        g.drawImage(Assets.base155, 675, 300);
        g.drawString("155mm", 675, 700, Assets.textPaint);
        g.drawImage(Assets.baseSOG, 975, 300);
        g.drawString("SOG", 975, 700, Assets.textPaint);

    }

    private void draw_Credits(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawString("Firebase Fables is a Static Charge Studios game", 20, 360, Assets.textPaint);
        g.drawString("Programming and Design by Michael Charge", 20, 400, Assets.textPaint);
        g.drawString("THIS IS AN ALPHA VERSION", 20, 440, Assets.textPaint);
        g.drawString("DO NOT DISTRIBUTE", 20, 480, Assets.textPaint);

    }

    private void draw_Options(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawString("Options to be put here", 20, 360, Assets.textPaint);

    }

    private void draw_FrontMenu(float deltaTime) {
        // Draw menu - Play, Options, Credits
        Graphics g = game.getGraphics();
        g.drawImage(Assets.PlayButton, 500, 200);
        g.drawImage(Assets.OptionsButton, 500, 350);
        g.drawImage(Assets.CreditsButton, 500, 500);
    }

    @Override
    public void pause() {
        if (Assets.menuMusic.isPlaying())
            Assets.menuMusic.pause();
    }

    @Override
    public void resume() {
        if (Assets.menuMusic.isPlaying() == false)
            Assets.menuMusic.play();
    }

    @Override
    public void dispose() {
        Assets.menuMusic.stop();
    }

    @Override
    public void backButton() {
        switch (state) {
            case Options:
                state = MenuState.FrontMenu;
                break;
            case Credits:
                state = MenuState.FrontMenu;
                break;
            case BaseSelect:
                state = MenuState.FrontMenu;
                break;
            case CommanderSelect:
                state = MenuState.BaseSelect;
                break;
            default:
                Assets.menuMusic.stop();
                android.os.Process.killProcess(android.os.Process.myPid());
                break;
        }
    }
}