package uk.StaticChargeStudios.FirebaseFables.game;

import uk.StaticChargeStudios.Framework.Interfaces.Game;
import uk.StaticChargeStudios.Framework.Interfaces.Graphics;
import uk.StaticChargeStudios.Framework.Interfaces.Screen;
import uk.StaticChargeStudios.Framework.Interfaces.Graphics.ImageFormat;


// Used to load up the image displayed during the main loading screen.
public class SplashLoadingScreen extends Screen {
    public SplashLoadingScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.splash= g.newImage("splash.jpg", ImageFormat.RGB565);
        
        
        game.setScreen(new LoadingScreen(game));

    }

    @Override
    public void draw(float deltaTime) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void backButton() {

    }
}