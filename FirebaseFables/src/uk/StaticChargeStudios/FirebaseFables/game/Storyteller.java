package uk.StaticChargeStudios.FirebaseFables.game;
///This class has one job - look at the list of events and select one for the next turn
// At launch it will open a story teller instance that will store the list of events and seperate storylines
public class Storyteller {
	
	public GameEvent currentEvent;

	public Storyteller(){
		
	}
	
	public void loadEvents(Assets.BaseType baseFile, Assets.CommanderType commanderFile){
        // look through
		
		// test for the moment
		if(!Assets.isTesting){
		currentEvent = new GameEvent();
		
		currentEvent.event1 = "Test";
		currentEvent.event2 = "Test testy test test";
		currentEvent.event3 = "Still test to see if the game can properly see if stuff is null";
		currentEvent.eventImage = Assets.commanderSOG;
		}

    }

    public void update() {

    }
}
