package uk.StaticChargeStudios.FirebaseFables.game;

import uk.StaticChargeStudios.Framework.Interfaces.Image;
import uk.StaticChargeStudios.Framework.Interfaces.Input.TouchEvent;

public class FF_Entity {
	
	int x, y, width, height;

    int targetX, targetY;

    Image image;

    boolean isActive;

    FF_Entity(){
        isActive = false;
    }
	
	public boolean inBounds(TouchEvent event) {
		if (event.x > x && event.x < x + width - 1 && event.y > y
				&& event.y < y + height - 1)
			return true;
		else
			return false;
	}

}


