package uk.StaticChargeStudios.FirebaseFables.game;

import uk.StaticChargeStudios.FirebaseFables.game.Assets.MapSymbolType;

//used for the symbols on the map - arty targets on the large scale map, enemy targets on small scale defence
public class FF_MapSymbol extends FF_Entity {
	
	public FF_MapSymbol() {
		
	}
	
	public void Initialize(MapSymbolType type) {
		
	}
	
	public void Update(){
		
	}

}
