package uk.StaticChargeStudios.FirebaseFables.game;

import java.util.List;

import android.graphics.Color;
import uk.StaticChargeStudios.Framework.Interfaces.Game;
import uk.StaticChargeStudios.Framework.Interfaces.Graphics;
import uk.StaticChargeStudios.Framework.Interfaces.Screen;
import uk.StaticChargeStudios.Framework.Interfaces.Input.TouchEvent;

public class GameScreen extends Screen {
	enum GameState {
		Loading, Briefing, EventPage, GamePage, Paused, GameOver
	}

	// Loading = Initial in screen/load up all the required entities
	// Ready = Briefing page appears
	// Running = TO BE REPLACED BY "EventPage" and "GamePage"
	// Paused = Only appears during the game page
	// GameOver = Final Briefing and stats

	GameState state = GameState.Loading;

	// Event Generator
	Storyteller eventMan;

	public GameScreen(Game game) {
		super(game);

		// Initialize game objects here
		eventMan = new Storyteller();
	}

	@Override
	public void update(float deltaTime) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

		if (state == GameState.Loading)
			updateLoading(touchEvents);
		if (state == GameState.Briefing)
			updateBriefing(touchEvents);
		if (state == GameState.EventPage)
			updateEventPage(touchEvents, deltaTime);
		if (state == GameState.GamePage)
			updateGamePage(touchEvents, deltaTime);
		if (state == GameState.Paused)
			updatePaused(touchEvents);
		if (state == GameState.GameOver)
			updateGameOver(touchEvents);
	}

	private void updateGamePage(List<TouchEvent> touchEvents, float deltaTime) {
		// TODO Auto-generated method stub

	}

	private void updateLoading(List<TouchEvent> touchEvents) {
		eventMan.loadEvents(Assets.currentBase, Assets.currentCommander);

		// Initalise all the units and such

		state = GameState.Briefing;
	}

	private void updateBriefing(List<TouchEvent> touchEvents) {

		// This example starts with a "Ready" screen.
		// When the user touches the screen, the game begins.
		// state now becomes GameState.Running.
		// Now the updateRunning() method will be called!

		if (touchEvents.size() > 0)
			state = GameState.EventPage;
	}

	private void updateEventPage(List<TouchEvent> touchEvents, float deltaTime) {

		// This is identical to the update() method from our Unit 2/3 game.
		// 2. Check miscellaneous events like death:
		// 3. Call individual update() methods here.
		// This is where all the game updates happen.
		// For example, robot.update();
	}

	private void updatePaused(List<TouchEvent> touchEvents) {
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {
				state = GameState.EventPage;
			}
		}
	}

	private void updateGameOver(List<TouchEvent> touchEvents) {
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {
				if (event.x > 300 && event.x < 980 && event.y > 100
						&& event.y < 500) {
					nullify();
					game.setScreen(new MainMenuScreen(game));
					return;
				}
			}
		}
	}

	@Override
	public void draw(float deltaTime) {
		// Graphics g = game.getGraphics();

		// First draw the game elements.

		// Example:
		// g.drawImage(Assets.background, 0, 0);
		// g.drawImage(Assets.character, characterX, characterY);

		// Secondly, draw the UI above the game elements.

		if (state == GameState.Loading)
			drawLoadingUI();
		if (state == GameState.Briefing)
			drawReadyUI();
		if (state == GameState.EventPage)
			drawEventPageUI();
		if (state == GameState.GamePage)
			drawGamePageUI();
		if (state == GameState.Paused)
			drawPausedUI();
		if (state == GameState.GameOver)
			drawGameOverUI();

	}

	private void drawGamePageUI() {
		// TODO Auto-generated method stub

	}

	private void drawLoadingUI() {
		Graphics g = game.getGraphics();

		g.drawImage(Assets.levelLoad, 0, 0);

	}

	private void nullify() {

		// Set all variables to null. You will be recreating them in the
		// constructor.
		eventMan = null;

		// Call garbage collector to clean up memory.
		System.gc();
	}

	private void drawReadyUI() {
		Graphics g = game.getGraphics();

		g.drawARGB(155, 0, 0, 0);
		g.drawString(Assets.currentBase.toString(), 640, 300, Assets.textPaint);

	}

	private void drawEventPageUI() {
		Graphics g = game.getGraphics();

		if (eventMan != null) {
			g.drawImage(eventMan.currentEvent.eventImage, 500, 100);
			g.drawString(eventMan.currentEvent.event1, 500, 400,
					Assets.textPaint);
			g.drawString(eventMan.currentEvent.event2, 500, 450,
					Assets.textPaint);
			if (eventMan.currentEvent.event3 != null) {
				g.drawString(eventMan.currentEvent.event3, 500, 500,
						Assets.textPaint);
			}
			if (eventMan.currentEvent.event4 != null) {
				g.drawString(eventMan.currentEvent.event4, 500, 550,
						Assets.textPaint);
			}
		}

	}

	private void drawPausedUI() {
		Graphics g = game.getGraphics();
		// Darken the entire screen so you can display the Paused screen.
		g.drawARGB(155, 0, 0, 0);

	}

	private void drawGameOverUI() {
		Graphics g = game.getGraphics();
		g.drawRect(0, 0, 1281, 801, Color.BLACK);
		g.drawString("GAME OVER.", 640, 300, Assets.textPaint);

	}

	@Override
	public void pause() {
		if (state == GameState.EventPage)
			state = GameState.Paused;

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}

	@Override
	public void backButton() {
		pause();
	}
}