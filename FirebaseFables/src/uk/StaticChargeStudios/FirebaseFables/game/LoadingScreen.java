package uk.StaticChargeStudios.FirebaseFables.game;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import uk.StaticChargeStudios.Framework.Interfaces.Game;
import uk.StaticChargeStudios.Framework.Interfaces.Graphics;
import uk.StaticChargeStudios.Framework.Interfaces.Screen;
import uk.StaticChargeStudios.Framework.Interfaces.Graphics.ImageFormat;


public class LoadingScreen extends Screen {
    public LoadingScreen(Game game) {
        super(game);
    }


    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.loadMenuMusic(game);
        //Image Loading
        Assets.menu = g.newImage("menuBack.png", ImageFormat.RGB565);
        Assets.levelLoad = g.newImage("loadingGame.png", ImageFormat.RGB565);
        // BaseSelectionLoad
        Assets.base105 = g.newImage("base105.png", ImageFormat.RGB565);
        Assets.base155 = g.newImage("base155.png", ImageFormat.RGB565);
        Assets.baseMortar = g.newImage("baseMortar.png", ImageFormat.RGB565);
        Assets.baseSOG = g.newImage("baseSF.png", ImageFormat.RGB565);
        // Commander Selection Load
        Assets.commanderInfantry = g.newImage("infantryCommander.png", ImageFormat.RGB565);
        Assets.commanderArty = g.newImage("artyCommander.png", ImageFormat.RGB565);
        Assets.commanderSOG = g.newImage("SOGCommander.png", ImageFormat.RGB565);
        // Buttons
        Assets.PlayButton = g.newImage("Start.png", ImageFormat.RGB565);
        Assets.OptionsButton = g.newImage("Options.png", ImageFormat.RGB565);
        Assets.CreditsButton = g.newImage("Credits.png", ImageFormat.RGB565);
        
        SetupTextPaint();
        SetupTitlePaint();

        game.setScreen(new MainMenuScreen(game));
    }


    private void SetupTextPaint() {
		Paint textPaint = new Paint();
        textPaint.setColor(Color.BLACK);
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(36);
        textPaint.setTypeface(Typeface.MONOSPACE);
		Assets.textPaint = textPaint;
	}

    private void SetupTitlePaint() {
        Paint textPaint = new Paint();
        textPaint.setColor(Color.BLACK);
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(48);
        textPaint.setTypeface(Typeface.MONOSPACE);
        Assets.titlePaint = textPaint;
    }


	@Override
    public void draw(float deltaTime) {
    	Graphics g = game.getGraphics();
		g.drawImage(Assets.splash, 0, 0);
    }


    @Override
    public void pause() {


    }


    @Override
    public void resume() {


    }


    @Override
    public void dispose() {


    }


    @Override
    public void backButton() {


    }
}