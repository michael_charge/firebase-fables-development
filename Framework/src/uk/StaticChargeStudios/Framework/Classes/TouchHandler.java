package uk.StaticChargeStudios.Framework.Classes;

import java.util.List;

import android.view.View.OnTouchListener;

import uk.StaticChargeStudios.Framework.Interfaces.Input.TouchEvent;

public interface TouchHandler extends OnTouchListener {
    public boolean isTouchDown(int pointer);
    
    public int getTouchX(int pointer);
    
    public int getTouchY(int pointer);
    
    public List<TouchEvent> getTouchEvents();
}
