package uk.StaticChargeStudios.Framework.Interfaces;

public interface Sound {
    public void play(float volume);

    public void dispose();
}
