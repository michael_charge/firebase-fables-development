package uk.StaticChargeStudios.Framework.Interfaces;

import uk.StaticChargeStudios.Framework.Interfaces.Graphics.ImageFormat;

public interface Image {
    public int getWidth();
    public int getHeight();
    public ImageFormat getFormat();
    public void dispose();
}
